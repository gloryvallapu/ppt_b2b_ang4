import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, } from 'ionic-angular';
import { AuthService } from "../../providers/auth-service/authService";
import { DashboardPage } from "../dashboard/dashboard";
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

import { CacheService } from "ionic-cache";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: Loading;
  // registerCredentials = { email: '', password: '' };
  credentials:any={};

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private cache: CacheService) {
    // cache.setOfflineInvalidate(false);
    // this.cache.clearAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.cache.clearAll();
  }

  
  public login() {
    this.showLoading()
    this.auth.login(this.credentials).subscribe(data => {
      if (data['status']=="success") {        
        this.navCtrl.setRoot(DashboardPage);
        localStorage.setItem('currentUser',JSON.stringify(data));
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  forgotPwd(){
    console.log("in");
    this.navCtrl.push("ForgotPasswordPage");
  }

}
