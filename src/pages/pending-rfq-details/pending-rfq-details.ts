import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';
import { FilterPipe } from '../../pipes/filter/filter';
import { ProductSplitDetailsPage } from "../product-split-details/product-split-details";

/**
 * Generated class for the PendingRfqDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-rfq-details',
  templateUrl: 'pending-rfq-details.html',
})
export class PendingRfqDetailsPage {
  userid: string;
  usertype: string;
  rfqdetails: any;
  moreRfqDataList: any = [];
  rfqnumber: any;
  moreRfqDetails: any;
  status: any;
  productList: any[];
  updatedProductList:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public myService: DealerServiceProvider, private global: GolablvariableProvider, public modalCtrl: ModalController) {
    this.moreRfqDetails = this.global.getpendingrfqObj();
    this.moreRfqDataList = this.moreRfqDetails.rfq_list;
    this.rfqnumber = this.moreRfqDetails.rfq_num;
  }


  ionViewDidLoad() {
    this.productList = [];
    console.log("moreRfqDataList",this.moreRfqDataList);
    
    for (let i = 0; i < this.moreRfqDataList.length; i++) {
      if (this.moreRfqDataList[i].product_status == 'Accept') {
        this.productList.push({ "product_name": this.moreRfqDataList[i].productname, "dealer_status": 'Confirm', "unique_nbr": "" });
      }
    }
  }

  getStatus(val, item) {
    this.status = val;
    item.dealer_status=this.status;
  }

  requestforpo() {
    this.setProductList();
    let data = {"rfq_num": this.rfqnumber, "data": this.updatedProductList, "user_type":"Dealer"}
    this.myService.dealersplitconfirm(data).subscribe((data) => {
        if (data.status == 'Success') {
            alert(data.status);
        }
    });
  };

  setProductList(){
    this.updatedProductList=[];
    let data={}
    for(let rfq of this.moreRfqDataList){
      if(rfq['product_status']=="Split"){
        data={};
        for(let split of rfq['split_info']){
         let splitData=this.setSplitDetails(split);
         data=splitData;
         data['product_name']=rfq['productname'];
         this.updatedProductList.push(data);
        }
      }
      else{
        data={};
        data['unique_nbr']=rfq['unique_nbr'];
        data['dealer_status']=rfq['dealer_status'];
        data['product_name']=rfq['productname'];
        if(rfq['dealer_status']=="Inprogress"){
          data['dealer_status']="Confirm";
        }
        this.updatedProductList.push(data);
      }
    }
  }

  setSplitDetails(split){
    let obj={}
    obj['unique_nbr']=split['unique_nbr'];
    obj['dealer_status']=split['product_status'];
    if(split['product_status']=="Split"){
      obj['dealer_status']="Confirm";
    }
    return obj
  }

  openmodal(item){
    let splitModal = this.modalCtrl.create(ProductSplitDetailsPage,{'list':item});
    splitModal.onDidDismiss(data => {
      this.setFinalProductList(data.data);
    });
    splitModal.present();
  }

  setFinalProductList(item){
    for(let product of this.moreRfqDataList){
      if(product.productid==item.productid){
        product=item;
      }
    }
  }

  disableButton(){
    for(let rfq of this.moreRfqDataList){
      if(rfq.product_status=='Pending for SM'){
        return true
      }
    }
    return false
  }

}
