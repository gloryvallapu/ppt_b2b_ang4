import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingRfqDetailsPage } from './pending-rfq-details';

@NgModule({
  declarations: [
    PendingRfqDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingRfqDetailsPage),
  ],
})
export class PendingRfqDetailsPageModule {}
