import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';

/**
 * Generated class for the ViewCartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-view-cart',
  templateUrl: 'view-cart.html',
})
export class ViewCartPage {
  userid: string;
  random_no: string;
  cartData: any;
  itemlist: any;
  rfqbtnshow: boolean;
  totalcart: any;
  qty: any;
  quantity: any;
  prodname: any;
  productname: any;
  cartemprty: boolean;
  cartindex: any;


  constructor(public navCtrl: NavController, public myService: DealerServiceProvider, public navParams: NavParams, public alertCtrl: AlertController, public loadingCtrl: LoadingController ) {
    this.viewcartData()
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewCartPage');
  }



  viewcartData() {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.userid = "5b5afdb2d5fe1f6a3223b4b9";
    this.random_no = '';

    this.myService.getviewcart(this.userid, this.random_no).subscribe((data) => {
      // console.log(data);
      if (data.status == 'Success') {
        loader.dismiss();
        this.cartData = data.total_item_list;
        this.totalcart = data.total_item_list.length;
        this.rfqbtnshow = true;
        this.cartemprty =false;
      } else if (data.status == 'cart is empyt.') {
        loader.dismiss();
        this.rfqbtnshow = false;
        this.cartemprty =true;
        this.cartData= [];
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: data['status'],
          buttons: ['OK']
        });
        alert.present();

      }
    })
  }

  generaterfqq(obj) {
    if(obj.length!=0){
      this.itemlist = obj;
      this.createrfq(obj);
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Cart is empty!",
        buttons: ['OK']
      });
      alert.present();
    }
  }

  createrfq(body) {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    body = { "product_info": this.itemlist, "userid": "5b5afdb2d5fe1f6a3223b4b9", "user_type": "Dealer" }
    this.myService.generateRfq(body).subscribe((data) => {
      //  console.log(data);
      if (data.status == 'success') {
        loader.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: "rfq generated successfully",
          buttons: ['OK']
        });
        alert.present();
      //  this.cartData = data.total_item_list;
        this.viewcartData();

      }
      else{
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: data['status'],
          buttons: ['OK']
        });
        alert.present();
      }
    })
  }

  decrement(obj, index) {
    this.qty = JSON.parse(obj.qty)
    if (obj.qty > 0) {
        this.qty = JSON.parse(obj.qty)
        this.qty -= JSON.parse(obj.pieceperCarton);
        obj.qty = JSON.stringify(this.qty);

        this.quantity = obj.qty;
        this.prodname = obj.upload_name;
        if (this.quantity >= 0) {
            this.getdistributorVoldiscount(this.quantity, this.prodname);
        }
    }
};

increment(obj, index) {
    console.log(index);
    this.qty = JSON.parse(obj.qty)
    this.qty += JSON.parse(obj.pieceperCarton);
    obj.qty = JSON.stringify(this.qty);
    this.quantity = obj.qty;
    this.prodname = obj.upload_name;
    if (this.quantity >= 1) {
        this.getdistributorVoldiscount(this.quantity, this.prodname);
    }
};


getdistributorVoldiscount = function (quantity, prodname) {
    this.myService.getvolumediscount(quantity, prodname).subscribe((data) => {
        if (data.status == 'Success') {
            console.log(data);
        }
        else{
          let counterror = this.alertCtrl.create({
            title: 'Error',
            subTitle: data['status'],
            buttons: ['OK']
          });
          counterror.present();
        }
    })
}

delete(prodname,index){
  this.productname = prodname;
  const confirm = this.alertCtrl.create({
    title: 'Delete carton?',
    message: 'Are you sure you want to delete this carton?',
    buttons: [
      {
        text: 'NO',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'YES',
        handler: () => {
          console.log('Agree clicked');
          this.deleteMethod(prodname,index);
        }
      }
    ]
  });
  confirm.present();
}

deleteMethod(prodname,index){
  const loader = this.loadingCtrl.create({
    content: "Please wait..."
  });
  loader.present();
  this.userid = "5b5afdb2d5fe1f6a3223b4b9";
  this.myService.deleteCartItems(this.productname, this.userid).subscribe((data) => {
    if (data.status == 'product deleted successfully') {
        //console.log(data);
        loader.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: data['status'],
          buttons: ['OK']
        });
        alert.present();
      //  this.cartData.splice(index ,1);
        this.viewcartData();
      
    }else{
      loader.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: data['status'],
        buttons: ['OK']
      });
    }
})
  
}






}
