import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';
import { ViewCartPage } from '../view-cart/view-cart';

//@IonicPage()
@Component({
    selector: 'page-rfq-details',
    templateUrl: 'rfq-details.html',
})
export class RfqDetailsPage {
    spclDiscountList: any;
    voldiscpercentage: any;
    voldiscount: any;
    selectData: any;
    msg: any;
    prodname: any;
    displayItem: boolean = false;
    values: any;
    userid: string;
    totalCartItems: any;
    qty: any;
    quantity: any;
    itemDisplayValu:boolean=false;
    item: any;
    item_index: any;
   
    constructor(public navCtrl: NavController, public navParams: NavParams, public myService: DealerServiceProvider, private global: GolablvariableProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController ) {
        this.selectData = this.global.getSelectedVal();
       // this.totalCartItems = this.global.getTotalCartItems();
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad RfqDetailsPage');
    }

    viewcart() {
        this.navCtrl.push(ViewCartPage);
    }


    moredetails(i,item) {
        this.item = item;
        this.item_index = i;
        console.log(this.item);  
    }
    addToCartIndividual(obj) {
        console.log(obj);
        if (obj.qty > 0) {
            this.values = [obj];
            this.userid = "5b5afdb2d5fe1f6a3223b4b9";
            this.addToCartInd(obj);
        } else {
            alert('Please Select Item Qty');
        }

    }
    addToCart(obj){
        if (obj.qty > 0) {
            this.values = [obj];
            this.userid = "5b5afdb2d5fe1f6a3223b4b9";
            this.addToCartInd(obj);
            return obj.qty=0
        } else {
            let error = this.alertCtrl.create({
                title: 'Error',
                subTitle: "Please add carton quantity",
                buttons: ['OK']
            });
            error.present();
        }
    }

    addToCartInd(body) {
        const loader = this.loadingCtrl.create({
            content: "Please wait..."
          });
          loader.present();
        body = { "orderitem": this.values, "user_id": this.userid, "order_status": "init" }
        this.myService.addtocart(body).subscribe((data) => {
            if (data.status == 'item added to cart') {
                loader.dismiss();
                let alert = this.alertCtrl.create({
                    title: 'Success',
                    subTitle: data['status'],
                    buttons: ['OK']
                  });
                  alert.present();
                this.totalCartItems = data.cart_count;
                this.global.setTotalCartItems(this.totalCartItems);
            }
        });
    };


    decrement(obj, index) {
        this.qty = JSON.parse(obj.qty)
        if (obj.qty > 0) {
            this.qty = JSON.parse(obj.qty)
            this.qty -= JSON.parse(obj.pieceperCarton);
            obj.qty = JSON.stringify(this.qty);

            this.quantity = obj.qty;
            this.prodname = obj.upload_name;
            if (this.quantity >= 0) {
                this.getdistributorVoldiscount(this.quantity, this.prodname, index);
            }
        }
    };

    increment(obj, index) {
        console.log(index);
        this.qty = JSON.parse(obj.qty)
        this.qty += JSON.parse(obj.pieceperCarton);
        obj.qty = JSON.stringify(this.qty);
        this.quantity = obj.qty;
        this.prodname = obj.upload_name;
        if (this.quantity >= 1) {
            this.getdistributorVoldiscount(this.quantity, this.prodname, index);
        }
    };


    getdistributorVoldiscount(quantity, prodname, index) {
        this.myService.getvolumediscount(quantity, prodname).subscribe((data) => {
            if (data.status == 'Success') {
                console.log(data);
                this.voldiscount = data.Percentage;
                this.voldiscpercentage = data.discount;
                this.spclDiscountList = data.product_info;
                this.selectData[index].special_discount = this.spclDiscountList[0].special_disc;
                this.selectData[index].volume_discount = this.spclDiscountList[0].vol_Percentage;
                this.selectData[index].total = this.spclDiscountList[0].total;
                this.selectData[index].saving_percentage = this.spclDiscountList[0].saving_percentage;
            }
            else{
                let counterror = this.alertCtrl.create({
                    title: 'Error',
                    subTitle: data['status'],
                    buttons: ['OK']
                  });
                  counterror.present();
            }
        })
    }



}
