import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';


/**
 * Generated class for the PendingRfqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-rfq',
  templateUrl: 'pending-rfq.html',
})
export class PendingRfqPage {
  userid: string;
  usertype: string;
  rfqdetails: any;
  moreRfqDataList:any=[];
  rfqnumber: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public myService: DealerServiceProvider,private global: GolablvariableProvider) {
    this.pendingRfq();
    // console.log(this.rfqnumber);
    
    

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PendingRfqPage');
  }

  pendingRfqDetails(obj) {
    // console.log(obj)
    this.rfqnumber = obj.rfq_num;
    this.global.setpendingrfqObj(obj);
    // console.log(this.global.getpendingrfqObj());

    // console.log(this.rfqnumber);
    this.navCtrl.push("PendingRfqDetailsPage");
    //this.pendingRfq();
  }


  pendingRfq() {
    this.userid = "5b5afdb2d5fe1f6a3223b4b9";
    this.usertype = "Dealer";
    this.myService.getpendingrfq(this.usertype, this.userid).subscribe((data) => {

      if (data.status == 'Success') {
        this.rfqdetails = data.more_info;
        
        // console.log(this.rfqdetails);
        this.rfqdetails.forEach(items => {
        
          items.rfq_list.forEach(item => {
            // console.log(item);
            this.moreRfqDataList.push(item);
           
          });
        });
       
      }

    })
  }


}
