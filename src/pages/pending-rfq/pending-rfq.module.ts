import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingRfqPage } from './pending-rfq';

@NgModule({
  declarations: [
    PendingRfqPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingRfqPage),
  ],
})
export class PendingRfqPageModule {}
