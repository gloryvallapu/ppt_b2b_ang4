import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthService } from "../../providers/auth-service/authService";


/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  credentials:any={};
  username:any;

  isValidUname:boolean=false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService) {
  }

  ionViewDidLoad() {
  }

  verifyUsername(){
    this.auth.forgotPassword(this.username).subscribe(data=>{
      if(data['status']=="success"){
        this.isValidUname=true
      }
    })
  }

  submitPwd(){
    this.credentials['username']=this.username;
      this.auth.updatePassword(this.credentials).subscribe(data=>{
        if(data['status']=="success"){
          this.back();
        }
      });
  }

  back(){
    this.navCtrl.push(LoginPage);
  }

}
