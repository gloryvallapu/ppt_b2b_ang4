import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';

/**
 * Generated class for the PendingPoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-po',
  templateUrl: 'pending-po.html',
})
export class PendingPoPage {
  userid: string;
  usertype: string;
  podetails: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public myService: DealerServiceProvider, private global:GolablvariableProvider) {
  this.pendingPo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingPoPage');
  }

  pendingPoDetails(obj){
    this.global.setPendingPoObj(obj);
    console.log(this.global.getPendingPoObj());
    this.navCtrl.push("PendingPoDetailsPage");

  }

  pendingPo() {
    this.userid = "5b5afdb2d5fe1f6a3223b4b9";
    this.usertype = "Dealer";
    this.myService.getpendingPodetails(this.usertype, this.userid).subscribe((data) => {
      console.log(data);

      if (data.status == 'Success') {
        this.podetails = data.more_info;  
        console.log(this.podetails);
        
       
      }

    })
  }

}
