import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingPoPage } from './pending-po';

@NgModule({
  declarations: [
    PendingPoPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingPoPage),
  ],
})
export class PendingPoPageModule {}
