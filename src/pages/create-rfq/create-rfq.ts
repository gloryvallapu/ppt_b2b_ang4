import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController ,AlertController, NavParams } from 'ionic-angular';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';
import { Body } from '@angular/http/src/body';
import { RfqDetailsPage } from '../rfq-details/rfq-details';

import { ViewCartPage } from '../view-cart/view-cart';

@IonicPage()
@Component({
  selector: 'page-create-rfq',
  templateUrl: 'create-rfq.html',
})
export class CreateRfqPage {

  multiCategoriesList: any;
  categories: any;
  dataObject: any;
  CategoryArray: any;
  SubCategoryArray: any;
  brandsArray: any;
  multiSubcatList: any;
  multibrandList: any;
  data1: {};
  getCategoryData: any;
  totalCartItems: any;
  categoryname:any=[];
  subcategory:any=[];
  brand:any=[];
 

  constructor(public navCtrl: NavController, public navParams: NavParams, public myService: DealerServiceProvider, private global: GolablvariableProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.totalCartItems = this.global.getTotalCartItems();
  }

  ionViewDidLoad() {
    this.myService.getCategories()
      .subscribe(data => {
        if (data.status == 'Success') {
          this.CategoryArray = data.categories;
          this.SubCategoryArray = data.subcategories;
          this.brandsArray = data.brands;
        }
      });
  }

  getcategory(data) {
    this.multiCategoriesList = data;
    this.getdistributorSubcatdata(data);
  };

  getdistributorSubcatdata(data) {
    data = { "category": data }
    this.myService.getSubCategories(data).subscribe((data) => {
      if (data.status == 'Success') {
        this.SubCategoryArray = data.subcategories;
        this.brandsArray = data.brands;
      }
    })
  }

  getsubcategory(data) {
    this.multiSubcatList = data;
    this.getBrands(data);
  };

  getBrands(body) {
    body = {};

  }

  getbrand(data) {
    this.multibrandList = data;
    // this.multibrandList.push(data);
  };

  checkAll(){
    if(this.brand.length==0||this.subcategory.length==0||this.categoryname.length==0){
      return false
    }
    return true
  }

  getdistributorcatgetdata(body) {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    if(this.checkAll()){
      body = { "category": this.multiCategoriesList, "sub_category": this.multiSubcatList, "brand": this.multibrandList, "vendor_code": "" }
      this.myService.getdistributortcatget(body).subscribe((data) => {
        if (data.status == 'Success') {
          loader.dismiss();
          this.getCategoryData = data.product_data;
          this.global.setSelectedVal(this.getCategoryData);
          this.navCtrl.push(RfqDetailsPage);
        }else{
          let error = this.alertCtrl.create({
            title: 'Error',
            subTitle: data['status'],
            buttons: ['OK']
          });
          error.present();
        }
      })
    }
    else{
      loader.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: "Please fill all details",
        buttons: ['OK']
      });
      alert.present();
    }
  }

  viewcart(){
    this.navCtrl.push(ViewCartPage);
  }

}



