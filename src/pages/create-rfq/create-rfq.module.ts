import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateRfqPage } from './create-rfq';

@NgModule({
  declarations: [
    CreateRfqPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateRfqPage),
  ],
})
export class CreateRfqPageModule {}
