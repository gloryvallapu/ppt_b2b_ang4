import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewCartPage } from '../view-cart/view-cart';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';
import { DealerServiceProvider } from '../../providers/dealer-service/dealer-service';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  totalCartItems: any;
  userid: any;
  wishlistItems: any;
  usertype: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private global: GolablvariableProvider, public myService: DealerServiceProvider) {
  //  this.totalCartItems = this.global.getTotalCartItems();
    this.userid = "5b5afdb2d5fe1f6a3223b4b9";
    this.usertype = "Dealer";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
    this.myService.getAllItemscount(this.userid, this.usertype).subscribe((data) => {
      if (data.status == "success") {
        this.totalCartItems = data.cart_count;
        this.wishlistItems = data.wishlist_count;
        this.global.setTotalCartItems(this.totalCartItems);
        console.log(this.global.getTotalCartItems());
      }
    });


  };

  viewcart() {
    this.navCtrl.push(ViewCartPage);
  }
  createRfq() {
    this.navCtrl.push("CreateRfqPage");
  }
  pendingRfq() {
    this.navCtrl.push("PendingRfqPage");
  }
  pendingPo() {
    this.navCtrl.push("PendingPoPage");
  }



}
