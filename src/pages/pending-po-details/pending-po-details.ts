import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GolablvariableProvider } from '../../providers/golablvariable/golablvariable';

/**
 * Generated class for the PendingPoDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-po-details',
  templateUrl: 'pending-po-details.html',
})
export class PendingPoDetailsPage {
  pendingPoDetails: any;
  pendingPoListData: any;
  po_num: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private global:GolablvariableProvider) {
    this.pendingPoDetails =this.global.getPendingPoObj();
    this.po_num = this.pendingPoDetails.po_num;
    console.log(this.pendingPoDetails.po_num)
    this.pendingPoListData = this.pendingPoDetails.rfq_list;
    console.log(this.pendingPoListData.length)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingPoDetailsPage');
  }

}
