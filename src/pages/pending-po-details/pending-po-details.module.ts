import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingPoDetailsPage } from './pending-po-details';

@NgModule({
  declarations: [
    PendingPoDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingPoDetailsPage),
  ],
})
export class PendingPoDetailsPageModule {}
