import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProductSplitDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-product-split-details',
  templateUrl: 'product-split-details.html',
})
export class ProductSplitDetailsPage {

  splitDatesList:any={}
  selectedRFQ:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController ) {
    this.selectedRFQ=navParams.get('list');
    this.splitDatesList=this.selectedRFQ['split_info'];
    this.setIndexing();
  }

  ionViewDidLoad() {
  }

  setIndexing(){
    let i=1
    for(let list of this.splitDatesList){
      list['SNO']=i++;
    }
  }

  dismiss() {
    let data={"data":this.selectedRFQ}
    this.viewCtrl.dismiss(data);
  }

  getStatus(val, item) {
    let status = val;
    item.product_status = status;
  }

}
