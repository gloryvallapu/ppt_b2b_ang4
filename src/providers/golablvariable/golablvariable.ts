// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GolablvariableProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GolablvariableProvider {

  selectedValue: any = [];
  SelectedRfqnumber: any;
  SelectedPO_number: any;
  totalCartItems: any;
  constructor() {
    console.log('Hello GolablvariableProvider Provider');
    this.selectedValue = "";
    this.SelectedRfqnumber = "";
    this.totalCartItems = '';
  }
  setSelectedVal(Val) {
    this.selectedValue = Val
  }
  getSelectedVal() {
    return this.selectedValue;
  }

  setpendingrfqObj(Val) {
    this.SelectedRfqnumber = Val
  }
  getpendingrfqObj() {
    return this.SelectedRfqnumber;
  }
  setPendingPoObj(Val) {
    this.SelectedPO_number = Val;
  }
  getPendingPoObj() {
    return this.SelectedPO_number;
  }
  setTotalCartItems(Val) {
    this.totalCartItems = Val;
  }
  getTotalCartItems() {
    return this.totalCartItems;
  }

}
