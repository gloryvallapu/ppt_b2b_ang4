import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

export class User {
  name: string;
  email: string;
  myHeaders: any;
  options: any
  data: any;

  constructor(name: string, email: string) {
    this.name = name;
    this.email = email;
  }
}

@Injectable()
export class AuthService {
  currentUser: User;
  myHeaders: any;
  options: any
  data: any;
  apiUrl = 'http://157.119.108.140';
  // apiUrl = 'http://192.168.20.65:80';
  constructor(public http: Http) {
    this.setHeaders();
  }

  setHeaders() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append("secret_key", "4r5t@W");
    headers.append('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');
    this.options = new RequestOptions({ headers: headers });
  }

  forgotPassword(username) {
    let obj = {
      username: username
    }
    return this.http.post(this.apiUrl + '/forgortpassword', obj, this.options).map(res => res.json());
  }

  updatePassword(data){
    let obj = {
      username: data['username'],
      password:data['pwd'],
      otp:data['otp']
    }
    return this.http.post(this.apiUrl + '/b_resetPassword', obj, this.options).map(res => res.json());
  }

  public login(credentials) {
    let obj = {}
    obj['username'] = credentials['email'];
    obj['password'] = credentials['password'];
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return this.http.post(this.apiUrl + '/powertex_login', obj, this.options).map(res => res.json());
    }
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getUserInfo() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  public logout() {
    let user = this.getUserInfo();
    let userToken = user['token'];
    let obj = {
      "token": userToken
    }
    return this.http.post(this.apiUrl + '/dealer_logout', obj, this.options).map(res => res.json());
  }

}