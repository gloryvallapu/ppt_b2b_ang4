// import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
// import { Header } from 'ionic-angular/umd/navigation/nav-interfaces';
import 'rxjs/add/operator/map';


@Injectable()
export class DealerServiceProvider {



  myHeaders: any;
  options: any
  data: any;
  
  constructor(public http: Http) {
    console.log('Hello DealerServiceProvider Provider');
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append("secret_key", "4r5t@W");
    headers.append('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');

    this.options = new RequestOptions({ headers: headers });
  
  }

  getUserId(){
    let user=JSON.parse(localStorage.getItem('currentUser'));
    let usernewId=user['dealer_info']['_id'];
    return usernewId['$oid']
  }

   apiUrl = 'http://157.119.108.140';

  // apiUrl = 'http://192.168.20.106:4444';
  // this.subBrand=this.apiUrl + '/catsubbrand'
  // apiUrl = 'http://192.168.20.107:4567';
  // apiUrl = 'http://192.168.20.65:80';



  getCategories() {
    return this.http.get(this.apiUrl + '/catsubbrand', this.options).map(res => res.json());
  };

  getdistributortcatget(body) {
    body['userid']=this.getUserId();
    return this.http.post(this.apiUrl + '/catsubcatbrandget', body, this.options).map(res => res.json());
  };

  getSubCategories(body) {
    return this.http.post(this.apiUrl + '/subbrand?category', body, this.options).map(res => res.json());
  };

  getbrands(body) {
    return this.http.post(this.apiUrl + '/sub_brand?subcategory', body, this.options).map(res => res.json());

  }
  addtocart(body) {
    return this.http.post(this.apiUrl + '/dealer_addtocart', body, this.options).map(res => res.json());
  }

  getviewcart(userId, random_no) {
    return this.http.get(this.apiUrl + '/dealer_addtocart?userid=' + userId + '&random_no=' + random_no, this.options).map(res => res.json());
  }
  deleteCartItems(prodname, userId){
    return this.http.delete(this.apiUrl + '/dealer_addtocart?userid=' + userId+'&product='+prodname,this.options).map(res => res.json());
  }

  generateRfq(body) {
    return this.http.post(this.apiUrl + '/dealer_order', body, this.options).map(res => res.json());
  }

  getpendingrfq(usertype, userid) {
    return this.http.get(this.apiUrl + '/rfq_get?usertype=' + usertype + '&userid=' + userid, this.options).map(res => res.json());
  }
  getpendingPodetails(usertype, userid) {
    return this.http.get(this.apiUrl + '/po_details?usertype=' + usertype + '&userid=' + userid, this.options).map(res => res.json());
  };

  getvolumediscount(qty, productname) {
    return this.http.get(this.apiUrl + '/volumediscount?qty=' + qty + '&upload_name=' + productname, this.options).map(res => res.json());
  };

  getAllItemscount(userid, usertype) {
    return this.http.get(this.apiUrl + '/wishadd_count?userid=' + userid + '&usertype=' + usertype, this.options).map(res => res.json());
  }
  dealersplitconfirm(body){
    return this.http.post(this.apiUrl + '/dealerorder_confirm', body, this.options).map(res => res.json());
  }
}




