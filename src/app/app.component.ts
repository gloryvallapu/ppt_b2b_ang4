import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { LoginPage } from '../pages/login/login';
import { CreateRfqPage } from "../pages/create-rfq/create-rfq";
import { PendingRfqPage } from "../pages/pending-rfq/pending-rfq";
import { PendingPoPage } from "../pages/pending-po/pending-po";
import { AuthService } from "../providers/auth-service/authService";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = DashboardPage;

  pages: Array<{title: string, component: any, icon:any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, private auth: AuthService, public splashScreen: SplashScreen) {
    // rootPage = LoginPage;
    this.initializeApp();
    if(!this.isLoggedIN()){
      this.rootPage=LoginPage;
    }
    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Home', component: DashboardPage,  icon:'../assets/imgs/web_icons/homeicon.png'  },
      {title: 'Create RFQ', component: "CreateRfqPage", icon:'../assets/imgs/web_icons/create_RFQ.png'  },
      {title: 'Pending RFQ', component: "PendingRfqPage", icon:'../assets/imgs/web_icons/Pending_RFQ.png'  },
      {title: 'Pending PO', component: "PendingPoPage", icon:'../assets/imgs/web_icons/Pending_PO.png'  }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  isLoggedIN(){
    if(this.auth.getUserInfo()){
      return true
    }
    return false
  }

  logout(){
    this.auth.logout().subscribe(data=>{
      console.log("data finally in app", data);
      if(data['status']=="success"){
        localStorage.clear();
        this.nav.setRoot(LoginPage);
      }
    })
  }
}
