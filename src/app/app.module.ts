import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DealerServiceProvider } from '../providers/dealer-service/dealer-service';
import { HttpModule } from '@angular/http';
import { GolablvariableProvider } from '../providers/golablvariable/golablvariable';
import { ViewCartPage } from '../pages/view-cart/view-cart';
import { RfqDetailsPage } from '../pages/rfq-details/rfq-details';
import {ProductSplitDetailsPage} from '../pages/product-split-details/product-split-details';
import { LoginPage } from '../pages/login/login';

import { CacheModule } from 'ionic-cache';

import { AuthService } from '../providers/auth-service/authService';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DashboardPage,
    ViewCartPage,
    RfqDetailsPage,
    ProductSplitDetailsPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    CacheModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DashboardPage,
    ViewCartPage,
    RfqDetailsPage,
    ProductSplitDetailsPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DealerServiceProvider,
    GolablvariableProvider,
    AuthService
  ]
})
export class AppModule {}
